#!/usr/bin/env groovy

def addTag(tags, tag) {
  if (tag?.trim()) {
    tags.add(tag)
  }
}

def evaluateReleaseTag(imageName) {
  def repoUtils = new pb.repo.local()

  def repoTags = repoUtils.getTags(repoUtils.getCurrentCommitId())
  def releaseTags = []

  for(itTag in repoTags) {
    // Expected format of the repo tag: `imageName/<imageTag>`
    def indexOfImage = itTag.indexOf(imageName + "/")
    if (0 <= indexOfImage)
    {
      def imageReleaseTag = itTag.substring(indexOfImage + imageName.length()).replaceAll('/', '').trim()
      if (imageReleaseTag) {
        addTag(releaseTags, imageReleaseTag)
      }
    }
  }
  return releaseTags
}

def call(Map input = [:]) {
  def config = [registry: 'https://index.docker.io',
                user: '',
                dockerImages: [],
                stableTag: '',
                outTags: [:]]
  config << input

  def dockerUtils = new pb.docker.utils()
  def dockerRegistry = new pb.docker.registryV2()

  for(itJob in config.dockerImages) {
    existingTags = []
    if (config.outTags[itJob.imageName]) {
      existingTags = config.outTags[itJob.imageName]
    }

    def imageId = dockerUtils.getImageId(config.user, itJob.imageName)
    def releaseTags = evaluateReleaseTag(itJob.imageName)

    // do not push tag if already pushed
    // Only add stable tag if there is a release tag
    releaseTags.removeAll { dockerRegistry.isTagPushed(config.registry, config.user, itJob.imageName, it) }
    if (0 < releaseTags.size()) {
      addTag(releaseTags, config.stableTag)
      config.outTags[itJob.imageName] = existingTags + releaseTags
    }
  }

  return config.outTags
}
