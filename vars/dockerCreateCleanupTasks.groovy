#!/usr/bin/env groovy

def removeUnusedContainers() {
  return {
    stage("Remove stopped containers") {
      sh "docker container prune --force"
    }
  }
}

def removeAllUnusedResources() {
  return {
    stage("Remove all unused images") {
      sh "docker system prune --force --all"
    }
  }
}

def call(Map input = [:]) {
  def config = [outCleanupTasks: [:]]
  config << input

  config.outCleanupTasks["containers"] = removeUnusedContainers()
  config.outCleanupTasks["images"] = removeAllUnusedResources()

  return config
}
