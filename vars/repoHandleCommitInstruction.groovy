#!/usr/bin/env groovy

def tagInstruction(args="") {
  def repoUtils = new pb.repo.local()
  def remoteRepoUtils = new pb.repo.remote()

  repoUtils.setCommitTag(args)
  remoteRepoUtils.pushTags()
}

def handleInstruction(instruction="", args="") {
  switch (instruction) {
    case "CI: tag":
      tagInstruction(args)
      break
  }
}

def call() {
  def repoUtils = new pb.repo.local()

  def commitMsg = repoUtils.getCurrentCommitMessage()
  def lines = commitMsg.split('\n')

  for(line in lines) {
    if (line.contains('CI: tag=')) {
      // CI: tag=jenkins/2.3
      def args = line.split('=')[1]
      handleInstruction('CI: tag', args.trim())
    }  
  }
}
