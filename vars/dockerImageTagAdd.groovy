#!/usr/bin/env groovy

// Adds a tag to all the images
// dockerImages: List of images to add a tag
// tags: List of tags to add
// outTags: In/out value, out value is same as return
def call(Map input = [:]) {
  def config = [dockerImages: [],
                tags: [],
                outTags: [:]]
  config << input

  for(itJob in config.dockerImages) {
    existingTags = []
    if (config.outTags[itJob.imageName]) {
      existingTags = config.outTags[itJob.imageName]
    }

    config.outTags[itJob.imageName] = existingTags + config.tags
  }

  return config.outTags
}
