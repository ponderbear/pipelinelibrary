#!/usr/bin/env groovy

def pushImage(imageId, tags = []) {
  return {
    stage("Push ${imageId}") {
      echo "Push image: ${imageId} to remote with tag ${tags}"

      tags.each {
        docker.image(imageId).push(it)
      }
    }
  }
}

def call(Map input = [:]) {
  def config = [user: '', dockerImages: [], imageTags: [:], outPushTasks: [:]]
  config << input

  def utils = new pb.docker.utils()

  for(itJob in config.dockerImages) {
    def imageId = utils.getTaggedBuildJobImageId(config.user, itJob.imageName)

    config.outPushTasks[itJob.imageName] = pushImage(imageId, config.imageTags[itJob.imageName])
  }

  return config.outPushTasks
}
