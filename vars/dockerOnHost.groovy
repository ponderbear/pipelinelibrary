#!/usr/bin/env groovy

def call(Map input = [:]) {
  def config = [serverConnection: '', serverCredentials: '', registryConnection: '', registryCredentials: '', callable: {}]
  config << input

  echo "On docker host with ${config}"

  docker.withServer(config.serverConnection, config.serverCredentials) {
    docker.withRegistry(config.registryConnection, config.registryCredentials) {
      config.callable()
    }
  }
}
