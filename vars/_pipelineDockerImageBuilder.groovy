#!/usr/bin/env groovy

def call(projectSettings) {
  // builds: Map of build tasks to be executed
  def dynamicProperties = [
    'remoteImageTags': [:],
    'buildTasks': [:],
    'pushTasks': [:],
    'cleanUpTasks': [:],
    'defaultBranch': projectSettings.repository.releaseBranch
  ]
  pipeline {
    agent any

    options {
      gitLabConnection('Default-Gitlab-Connection')
      gitlabBuilds(builds: projectSettings.pipeline.buildSteps)
    }

    stages {
      stage('Checkout') {
        parallel {

          stage('SimpleCheckout') {
            when { not { expression { gitlabIsMergeRequest() } } }
            steps {
              gitCheckoutSimple(
                repoUrl: projectSettings.repository.url,
                credentialsId: projectSettings.repository.credentials,
                branch: repoRemoteGetCurrentBranch(dynamicProperties['defaultBranch'])
              )
            }
          }

          stage('MergeCheckout') {
            when { expression { gitlabIsMergeRequest() } }
            steps {
              echo "'Merge request detected ' ${env.gitlabMergeRequestId}"
              gitlabStepUpdate(projectSettings.pipeline.buildSteps, projectSettings.pipeline.mergeStepName, 'running')

              gitCheckoutMerge(
                repoUrl: projectSettings.repository.url,
                credentialsId: projectSettings.repository.credentials,
                branch: repoRemoteGetSourceBranch(),
                userName: 'jenkins', userEmail: 'jenkins@snowflurry.fun',
                targetBranch: repoRemoteGetTargetBranch(),
                targetRemote: 'origin'
              )
            }
          }
        }
        post {
          success {
            gitlabStepUpdate(projectSettings.pipeline.buildSteps, STAGE_NAME, 'success')
          }
          failure {
            gitlabStepUpdate(projectSettings.pipeline.buildSteps, STAGE_NAME, 'failed')
          }
        }
      }

      stage('Build') {
        steps {
          gitlabStepUpdate(projectSettings.pipeline.buildSteps, STAGE_NAME, 'running')

          echo "gitlabBranch: ${env.gitlabBranch}"
          echo "gitlabActionType: ${env.gitlabActionType}"
          echo "repoRemoteGetCurrentBranch: ${repoRemoteGetCurrentBranch()}"
          echo "repoLocalGetCurrentCommitId: ${repoLocalGetCurrentCommitId()}"
          echo "repoLocalGetCurrentBranch: ${repoLocalGetCurrentBranch()}"

          dockerCreateBuildTasks(
            user: projectSettings.dockerRegistry.user,
            dockerImages: projectSettings.dockerImages,
            outBuildTasks: dynamicProperties['buildTasks']
          )

          dockerOnHost(
            serverConnection: env.DEFAULT_DOCKER_HOST_CONNECTION, serverCredentials: 'default-docker-host-credentials',
            registryConnection: env.DEFAULT_DOCKER_REGISTRY_CONNECTION, registryCredentials: 'default-docker-registry-credentials',
            callable: {
              for (task in dynamicProperties['buildTasks'].values()) {
                task.call()
              }
            }
          )
        }
        post {
          success {
            gitlabStepUpdate(projectSettings.pipeline.buildSteps, STAGE_NAME, 'success')
          }
          failure {
            gitlabStepUpdate(projectSettings.pipeline.buildSteps, STAGE_NAME, 'failed')
          }
        }
      }

      stage('Push') {
        stages {
          stage('SetupBuildTags') {
            steps {
              dockerImageTagAdd(
                dockerImages: projectSettings.dockerImages,
                tags: [projectSettings.dockerRegistry.buildTag],
                outTags: dynamicProperties['remoteImageTags']
              )
            }
          }

          stage('SetupReleaseTags') {
            when { 
              not { expression { gitlabIsMergeRequest() } }
              expression { repoLocalIsOnBranchContaining(name: projectSettings.repository.releaseBranch) }
            }
            steps {
              dockerImageTagAdd(
                dockerImages: projectSettings.dockerImages,
                tags: [projectSettings.dockerRegistry.latestTag],
                outTags: dynamicProperties['remoteImageTags']
              )
              dockerImageTagEvaluateRelease(
                registry: projectSettings.dockerRegistry.url,
                user: projectSettings.dockerRegistry.user,
                dockerImages: projectSettings.dockerImages,
                stableTag: projectSettings.dockerRegistry.releaseTag,
                outTags: dynamicProperties['remoteImageTags']
              )
            }
          }
          stage('SetupPushTasks') {
            steps {
              dockerCreatePushTasks(
                user: projectSettings.dockerRegistry.user,
                dockerImages: projectSettings.dockerImages,
                imageTags: dynamicProperties['remoteImageTags'],
                outPushTasks: dynamicProperties['pushTasks']
              )
            }
          }
          stage('PushToRegistry') {
            steps {
              dockerOnHost(
                serverConnection: env.DEFAULT_DOCKER_HOST_CONNECTION, serverCredentials: 'default-docker-host-credentials',
                registryConnection: env.DEFAULT_DOCKER_REGISTRY_CONNECTION, registryCredentials: 'default-docker-registry-credentials',
                callable: {
                  parallel dynamicProperties['pushTasks']
                }
              )
            }
          }
        }
        post {
          success {
            gitlabStepUpdate(projectSettings.pipeline.buildSteps, STAGE_NAME, 'success')
          }
          failure {
            gitlabStepUpdate(projectSettings.pipeline.buildSteps, STAGE_NAME, 'failed')
          }
        }
      }

      stage('Release') {
        when { 
          not { expression { gitlabIsMergeRequest() } }
          not { expression { repoLocalHasTags() } }
          expression { repoLocalIsOnBranchContaining(name: projectSettings.repository.releaseBranch) }
          expression { repoHasCommitInstructions() }          
        }

        steps {
          gitlabStepUpdate(projectSettings.pipeline.buildSteps, STAGE_NAME, 'running')

          sshagent (credentials: ['jenkins-snowflurry']) {
            repoHandleCommitInstruction()
          }
        }
        post {
          success {
            gitlabStepUpdate(projectSettings.pipeline.buildSteps, STAGE_NAME, 'success')
          }
          failure {
            gitlabStepUpdate(projectSettings.pipeline.buildSteps, STAGE_NAME, 'failed')
          }
        }
      }
    }
    post {
      success {
        gitlabStepUpdate(projectSettings.pipeline.buildSteps, projectSettings.pipeline.mergeStepName, 'success')
        script {
          if (gitlabIsMergeRequest()) {
            addGitLabMRComment comment: "Build number: ${env.BUILD_NUMBER}.\n\
              Merge to ${env.gitlabTargetBranch} successful.\n\
              Build hash: ${repoLocalGetCurrentCommitId()}"
          }
        }
      }
      failure {
        gitlabStepUpdate(projectSettings.pipeline.buildSteps, projectSettings.pipeline.mergeStepName, 'failed')
      }
      cleanup {
        script {
          stage('CleanUp') {
            def baseStageName = STAGE_NAME
            catchError(stageResult: 'FAILURE', buildResult: 'FAILURE') {

              gitlabStepUpdate(projectSettings.pipeline.buildSteps, baseStageName, 'running')
              // remove build step because it is running now
              projectSettings.pipeline.buildSteps.remove(baseStageName)

              dockerCreateCleanupTasks(outCleanupTasks: dynamicProperties['cleanUpTasks'])

              dockerOnHost(
                serverConnection: env.DEFAULT_DOCKER_HOST_CONNECTION, serverCredentials: 'default-docker-host-credentials',
                registryConnection: env.DEFAULT_DOCKER_REGISTRY_CONNECTION, registryCredentials: 'default-docker-registry-credentials',
                callable: {
                  for (task in dynamicProperties['cleanUpTasks'].values()) {
                    task.call()
                  }
                }
              )

              stage('CheckPendingBuilds') {
                // Set pending builds as failed (except removed 'cleanUp')
                if (projectSettings.pipeline.buildSteps.size() > 0) {
                  def pendingJobs = projectSettings.pipeline.buildSteps
                  // Mark the build step as failed in Jenkins and on gitlab, error aborts and marks the build as failed
                  gitlabStepSetAll(projectSettings.pipeline.buildSteps, 'canceled')
                  gitlabStepUpdate(projectSettings.pipeline.buildSteps, baseStageName, 'failed')

                  error("Pending build jobs are not finished: ${pendingJobs}")
                }
                else {
                  gitlabStepUpdate(projectSettings.pipeline.buildSteps, baseStageName, 'success')
                }
              }

              stage('WorkspaceCleanup') {
                // clean up workspace
                cleanWs()
              }
            }
          }
        }
      }
    }
  }
}
