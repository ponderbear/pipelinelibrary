#!/usr/bin/env groovy

def call(defaultBranch = '') {
  if (env.BRANCH_NAME) {
    return env.BRANCH_NAME
  }
  else if (env.gitlabBranch) {
    return env.gitlabBranch
  }
  else if (env.gitlabSourceBranch) {
    return env.gitlabSourceBranch
  }

  return defaultBranch
}
