#!/usr/bin/env groovy

def call() {
  def utils = new pb.repo.local()
  return utils.getCurrentCommitId()
}
