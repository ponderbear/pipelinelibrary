package pb.repo

def getTags(commit = getCurrentCommitId()) {
    if (commit) {
        def desc = sh(script: "git tag -l --points-at ${commit}", returnStdout: true)?.trim()
        return desc.split()
    }
    return null
}

def getCurrentCommitId() {
    return sh(script: 'git rev-parse HEAD', returnStdout: true)?.trim()
}

def getCurrentBranches(commit = getCurrentCommitId()) {
  if (commit) {
      def desc = sh(script: "git branch -l --points-at ${commit}", returnStdout: true)?.trim()
      return desc
  }
  return null
}

def isCommitOnBranch(branchName, commit = getCurrentCommitId()) {
  if (commit && branchName) {
    def desc = sh(script: "git branch --contains ${commit}", returnStdout: true)?.trim()
    return desc.contains(branchName)
  }
  return false
}

def isCurrentBranchContaining(branchName) {
  return getCurrentBranch().contains(branchName)
}

def getCurrentBranch() {
  return sh(script: 'git rev-parse --abbrev-ref HEAD', returnStdout: true)?.trim()
}

def getCurrentCommitMessage() {
  return sh(script: 'git show -s', returnStdout: true)?.trim()
}

def setCommitTag(tagValue) {
  return sh(script: "git tag '${tagValue}'", returnStdout: true)?.trim()
}