package pb.build

def getBuildNumber() {
  if (env.BUILD_NUMBER != null) {
    return env.BUILD_NUMBER
  }
  return '0'
}
