#!/usr/bin/env groovy

package pb.docker

def isImageProcessingRequired(currentImageName, jobList) {
  // If the current branch contains an image name, only this image is built
  // Otherwise all images are built

  for (itImageJob in jobList) {
    if (repositoryUtils.containsCurrentBranch(itImageJob.imageName) == true) {
      // The branch name contains one of the image names
      // Check whether the current image is the one to build
      return repositoryUtils.containsCurrentBranch(currentImageName)
    }
  }

  // Branch does not contain an image name => build all images
  return true
}

def evaluateBuildJobTag() {
  def build = new pb.build.utils()
  def repo = new pb.repo.local()

  // Get the branch from the remote
  // If no branch is available, fallback to the currently checked out branch
  def currentBranch = repo.getCurrentBranch()

  return "${currentBranch}-b${build.getBuildNumber()}".replaceAll('/', '-')
}

def getTaggedBuildJobImageId(user, imageName) {
  def buildTag = evaluateBuildJobTag()
  return "${user}/${imageName}:${buildTag}"
}

def getImageId(user, imageName) {
  return "${user}/${imageName}"
}
