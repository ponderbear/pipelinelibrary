#!/usr/bin/env groovy

package pb.docker

def isTagPushed(registry = '', user='', image = '', tag = '') {
  def response = httpRequest url: "${registry}/v2/namespaces/${user}/repositories/${image}/tags/${tag}", validResponseCodes: '200,404'

  echo "docker registry tag response: ${response.status}"
  echo "docker registry tag response: ${response.content}"

  return (response.status == 200)
}
