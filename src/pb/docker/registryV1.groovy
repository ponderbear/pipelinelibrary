#!/usr/bin/env groovy

package pb.docker

def getTags(registry = '', user = '', image = '') {
  def response = httpRequest "${registry}/v1/repositories/${image}/tags"
  def jsonData = readJSON text: response.content

  def tags = []
  jsonData.each {
    tags.add(it.name)
  }

  return tags
}

def isTagPushed(registry = '', user = '', image = '', tag = '') {
  return getTags(registry, image).find{ element -> element == tag}
}
